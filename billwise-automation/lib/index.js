'use strict';

let prefix = require('../config').apiPrefix;

exports.register = (server, options, next) => {

  prefix = prefix === '' ? undefined : prefix;

    server.register([
      {
        register: require('../runtime')
      },
      // {
      //   register: require('../db')
      // },
      {
        register: require('../auth')
      },
      // {
      //   register: require('../methods')
      // },
      // {
      //   register: require('../services')
      // },
      // {
      //   register: require('../jobs')
      // },
      {
        register: require('./shutdown')
      },
      {
        register : require('../app/headless-chrome'),
      }
    ], (err) => {

      if (err) {
        return next(err);
      }
    });
  next();
};

exports.register.attributes = {
  pkg: require('../package.json')
};
