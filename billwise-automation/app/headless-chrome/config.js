'use strict';

const validations = require('./validations');
const handler = require('./handler');

module.exports = {
  create: {
    //validate: validations.create,
    pre: [
      {
        method: 'consumer.create(payload)', assign: 'createConsumer'
      }
    ],
    handler: handler.create
  },
  createFromCSV: {
    //validate: validations.create,
    pre: [
      {
        method: 'consumer.createFromCSV(payload)', assign: 'createConsumerFromCSV'
      }
    ],
    handler: handler.createFromCSV
  }
}
