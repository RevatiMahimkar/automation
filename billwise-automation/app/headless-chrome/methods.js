'use strict';
const Papa = require('papaparse');
const puppeteer = require('puppeteer');
const addConsumerDscom = require('./script/object');

module.exports = (server, options) => [
  {
    name: 'consumer.create',
    method: createConsumer,
    options: {
      // bind : server.waterline.collections.consumers
    }
  },
  {
      name: 'consumer.createFromCSV',
      method: createFromCSV,
      options: {
        // bind : server.waterline.collections.consumers
      }
  }
]

const createConsumer = function createConsumer(payload, next) {

  addConsumer(payload, (err, result) => {
    if(err) return next(err);
    return next(null, result);
  });
}

const createFromCSV = function createFromCSV(payload, next) {
    let  consumers;
    Papa.parse(payload.file, {
        header: true,
	      complete: function(results) {
          console.log(results.data);
          consumers = results.data;
	      }
    });
    addConsumer(consumers, (err, result) => {
      if(err) return next(err);
      return next(null, result);
    });
}

const addConsumer = async function addConsumer(consumers, next) {
  console.log('Launching Browser');
  const browser = await puppeteer.launch({
    headless: false,
    slowMo: 100,
    timeout : 0
  });
  const page = await browser.newPage();

  const responses = [];
  page.on('response', resp => {
    responses.push(resp);
  });

  // console.log(JSON.stringify(responses, null, 8));

  console.log('Visiting Page');
  // Open page.
  await page.goto('https://wss.mahadiscom.in/wss/wss?uiActionName=getCustAccountLogin');

  await page.setViewport({
    width: 1920,
    height: 1080
  });
  // console.log(consumers);

  console.log('entering username');
  await page.type('#loginId','tmc.billwise');

  console.log('entering password');
  await page.type('#password','Selenite#1');

  console.log('Clicking on Login Button');
  await page.click('#lblLoginButton');

  console.log('waiting for page navigation');
  await page.waitFor('#consumerType');
  console.log('login successful');

  addConsumerToList(consumers);

  async function addConsumerToList(consumers) {
    if(!consumers.length) {
      await browser.close();
      return next(null,'done');
    }
    let value;
    // console.log("before is array");

    if (Array.isArray(consumers)) {
      // console.log("from is array");
      value = consumers.shift();
    } else {
      value = consumers;
      consumers = {};
    }
    if(value.connectionType && value.consumerNumber && value.billingUnit) {
      console.log("Adding consumer ", value.consumerNumber);
      await page.type('#consumerType', value['connectionType']);
      await page.type('#consumerNumber', value['consumerNumber']);
      await page.type('#' + (value['connectionType'] === 'LT Consumer' ? 'BU' : 'ddlCircleCode'), value['billingUnit']);
      const result = await page.evaluate(addConsumerDscom);
      console.log(result);
      await page.waitFor(2000);
      addConsumerToList(consumers);
    } else {
      await browser.close();
      return next(null,'done');
    }
  }
}
