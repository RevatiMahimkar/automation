'use strict';

const Joi = require('joi');

module.exports = {
  create: {
    payload: {
      connectionType: Joi.string().valid(['LT Consumer','HT Consumer']).required(),
      billingUnit: Joi.string().min(3).max(4).required(),
      consumerNumber: Joi.string().min(12).max(12).required()
    }
  }
}
