'use strict';

module.exports = {
  create : (request, reply) => reply(request.pre.createConsumer),
  createFromCSV : (request, reply) => reply(request.pre.createConsumerFromCSV)

}
