'use strict';

exports.register = function(server, options, next){
  let methods = require('./methods');
  server.method(methods(server, options));
  let routes = require('./routes');
  server.route(routes(server, options));
  next();
};

exports.register.attributes = {
  name : 'hapi-headless-chrome'
};
