'use strict';

let config = require('./config');

module.exports = (server, options) => {

  return [
    {
      method: 'POST',
      path: '/consumer',
      config: config.create
    },
    {
      method: 'POST',
      path: '/consumer/csv',
      config: config.createFromCSV
    }
  ];
}
