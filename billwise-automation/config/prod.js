'use strict';

module.exports = {
  env: 'production',

  product: {
    name: 'billwiseAutomation'
  },

  server: {
    billwiseAutomation: {
      host: '0.0.0.0',
      port: process.env.PORT || 3000,
      tls: false
    }
  },

  chairo: {
    options: {
      // prevent seneca timeout error
      // https://github.com/senecajs/seneca-transport/issues/23
      timeout: 999999999
    }
  },

  cache: {
    mainCache: {
      engine: 'catbox-memory',
      name: 'mainCache',
      partition: 'billwise-automation'
    }
  },

  dogwater: {
    connections: {
      myApi: {
        adapter: 'sails-mongo',
        host: 'localhost',
        port: 27017,
        database: 'my-api'
      }
    },
    adapters: {
      'sails-mongo': require('sails-mongo')
    },
    models: require('path').resolve(__dirname, '..', 'models')
  },

  good: {
    ops: {
      interval: 1000
    },
    reporters: {
      myConsoleReporter: [{
        module: 'good-squeeze',
        name: 'Squeeze',
        args: [{ log: '*', request: '*', response: '*', error: '*' }]
      }, {
        module: 'good-console'
      }, 'stdout'],
      myFileReporter: [{
        module: 'good-squeeze',
        name: 'Squeeze',
        args: [{ log: '*', request: '*', response: '*', error: '*' }]
      }, {
        module: 'good-squeeze',
        name: 'SafeJson'
      }, {
        module: 'good-file',
        args: ['./logs/prod.log']
      }]
    }
  },

  debug: {
    request: ['error']
  },

  jobs: {
    address: 'mongodb://localhost:27017/my-api',
    collection: 'jobs',
    frequency: '5 minutes',
    concurrency: 20
  },

  apiPrefix: '/api/v1',

  deploy: {
    branch: 'master',
    servers: 'admin@vm-address-here.com',
    deployTo: '/home/admin/billwiseAutomation'
  }
};
