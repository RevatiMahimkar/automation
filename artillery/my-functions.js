
  // my-functions.js

  module.exports = {
    setJSONBody: setJSONBody,
    setContextVariables:setContextVariables,
    logHeaders: logHeaders,
    setURLParams : setURLParams
  }

  function setJSONBody(requestParams, context, ee, next) {
    return next(); // MUST be called for the scenario to continue
  }

  function logHeaders(requestParams, response, context, ee, next) {

    console.log(requestParams);

    return next(); // MUST be called for the scenario to continue
  }

  function setContextVariables(requestParams, response, context, ee, next) {

    var heads = JSON.parse(response.body);
    if(Array.isArray(heads)) { 
      context.vars.head_id = heads[0].id; 
    }

    return next(); // MUST be called for the scenario to continue
  }

function setURLParams(requestParams, context, ee, next) {
  console.log(context.vars["head_id"]);

  return next(); // MUST be called for the scenario to continue
}